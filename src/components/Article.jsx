// @flow

import React from 'react';

type ArticleProps = {
  url: string,
  title: string
}

const Article = ({ url, title }: ArticleProps) => (
  <a href={url}>
    {title}
  </a>
);

export default Article;
