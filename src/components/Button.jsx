// @flow

import React from 'react';

import { Button as AntdButton } from 'antd';

type ButtonProps = {
  value: string,
  onClick?: any,
  loading?: boolean,
  disabled?: boolean,
  danger?: boolean
}

const Button = ({
  onClick,
  value,
  loading,
  disabled,
  danger,
}: ButtonProps) => (
  <AntdButton
    type="primary"
    onClick={onClick}
    loading={loading}
    disabled={disabled}
    danger={danger}
  >
    {value}
  </AntdButton>
);

Button.defaultProps = {
  loading: false,
  disabled: false,
  danger: false,
  onClick: null,
};

export default Button;
