// @flow

import React from 'react';

import { Input as AntdInput } from 'antd';

type SearchProps = {
  placeholder: string,
  loading?: boolean,
}

const { Search: AntdSearch } = AntdInput;

const Search = ({ placeholder, loading }: SearchProps) => (
  <AntdSearch
    placeholder={placeholder}
    loading={loading}
  />
);

Search.defaultProps = {
  loading: false,
};

export default Search;
