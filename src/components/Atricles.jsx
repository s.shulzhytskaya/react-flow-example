// @flow

import React from 'react';

import { List as AntdList } from 'antd';

import Article from './Article';

type ArticleEntity = {
  url: string,
  title: string,
  id: string
}

type ArticlesProps = {
  articles: Array<ArticleEntity>
}

const Articles = ({ articles }: ArticlesProps) => (
  <AntdList
    size="large"
    bordered
    dataSource={articles}
    renderItem={({ title, url }) => (
      <AntdList.Item>
        <AntdList.Item.Meta title={<Article url={url} title={title} />} />
      </AntdList.Item>
    )}
  />
);

export default Articles;
