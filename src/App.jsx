import React from 'react';

import {
  Layout,
  Row,
  Col,
  Space,
} from 'antd';

import Button from './components/Button';
import Articles from './components/Atricles';
import Search from './components/Search';

import 'antd/dist/antd.css';
import './App.css';

const ARTICLES = [
  { id: '0', title: 'Article 1', url: '#' },
  { id: '1', title: 'Article 2', url: '#' },
  { id: '2', title: 'Article 3', url: '#' },
  { id: '3', title: 'Article 4', url: '#' },
  { id: '4', title: 'Article 5', url: '#' },
  { id: '5', title: 'Article 6', url: '#' },
];
const { Header, Footer, Content } = Layout;

function App() {
  return (
    <Layout style={{ height: '100vh' }}>
      <Header style={{ color: 'white' }}>Header</Header>
      <Content>
        <Space direction="vertical">
          <Row>
            <Col span="24">
              <Articles articles={ARTICLES} />
            </Col>
          </Row>
          <Row>
            <Col span="24">
              <Search placeholder="Search smth" />
            </Col>
          </Row>
          <Row>
            <Col span="2">
              <Button value="Click me!" />
            </Col>
          </Row>
        </Space>
      </Content>
      <Footer style={{ textAlign: 'center' }}>React flow example</Footer>
    </Layout>
  );
}

export default App;
